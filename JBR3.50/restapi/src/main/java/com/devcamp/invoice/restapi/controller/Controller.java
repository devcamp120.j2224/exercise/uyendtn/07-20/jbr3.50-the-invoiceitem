package com.devcamp.invoice.restapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.invoice.restapi.model.InvoiceItem;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("invoices")
    public ArrayList<InvoiceItem> invoiceList() {
        InvoiceItem invoice1 = new InvoiceItem("TB1", "table", 100, 500000);
        InvoiceItem invoice2 = new InvoiceItem("CH1", "chair", 500, 100000);
        InvoiceItem invoice3 = new InvoiceItem("UB1", "umbrella", 100, 5000000);

        System.out.println(invoice1.toString());
        System.out.println(invoice2.toString());
        System.out.println(invoice3.toString());

        ArrayList<InvoiceItem> invoiceList = new ArrayList<InvoiceItem>();
        invoiceList.add(invoice1);
        invoiceList.add(invoice2);
        invoiceList.add(invoice3);

        return invoiceList;
    }
}
